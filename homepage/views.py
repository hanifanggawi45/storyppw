from django.shortcuts import render

# Create your views here.

def profile(request):
    return render(request, "homepage/profile.html")

def fitur(request):
    return render(request, "homepage/fitur.html")

