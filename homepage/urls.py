from django.urls import path
from . import views

# app_name = 'homepage'

urlpatterns = [
    path('', views.profile, name= 'profile'),
    path('fitur',views.fitur,name='fitur'),
]